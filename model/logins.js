var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var loginSchema = new Schema({
  username: {type: String, unique: true},
  password: String,
  user_type: String,
  created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Login', loginSchema);