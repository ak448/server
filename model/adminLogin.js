var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var loginSchema = new Schema({
  username: String,
  password: String,
  created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Admin', loginSchema);