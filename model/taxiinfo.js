var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var taxiSchema = new Schema({
  company: String,
  pickup_location: String,
  taxi_number: String,
  taxi_make: String,
  taxi_model: String,
  year_vehicle: String,
  miles: Number,
  capacity: Number,
  handicapped: String,
  lease_amount: String,
  full_time_work: String,
  lat: String,
  lang: String
});

module.exports = mongoose.model('Taxiinfo', taxiSchema);