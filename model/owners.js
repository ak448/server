var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var ownerSchema = new Schema({
  name: String,
  username: {type:String, unique:true},
  password: String,
  contact: Number,
  email: String,
  shift: String,
  comments: String,
  photo: String,
  taxi_info: [{type: ObjectId, ref: 'Taxiinfo'}],
  status: {type: Boolean, default: true},
  created_at: {type: Date, default: new Date()}
});

module.exports = mongoose.model('Owner', ownerSchema);