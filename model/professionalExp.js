var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var driverExperience = new Schema({
  preferredTaxi: { type: String, required: true},
  experinceOfTaxi:{ type: String},
  violations: String,
  hoursAvailability: String,
  daysAvailabity:String,
  shift: String,
});

module.exports = mongoose.model('ProfExperience', driverExperience);